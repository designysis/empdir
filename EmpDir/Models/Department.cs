﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmpDir
{
    public partial class Department
    {
        public Department()
        {
            Employees = new HashSet<Employee>();
        }

        public long DepartmentId { get; set; }

        [Display(Name = "Department Name")]
        public string DepartmentName { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}

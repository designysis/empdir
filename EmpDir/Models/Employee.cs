﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmpDir
{
    public partial class Employee
    {
        public long EmployeeId { get; set; }

        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        public string Phone { get; set; }

        [Display(Name = "Contact Info")]
        public string ContactInfo { get; set; }

        [Display(Name = "Department")]
        public long? DepartmentId { get; set; }

        public virtual Department Department { get; set; }
    }
}

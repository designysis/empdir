# EmpDir

Coding exercise - a simple web application for managing employee contact lists.

Either open the solution file (EmpDir.sln) in Visual Studio 2019 or build and run from the command line.

Build:
>	cd <repo root folder></repo>
>	dotnet build -c Debug

--this will build the project in Debug mode into EmpDir\bin\debug\netcoreapp2.2

Run:
> cd EmpDir\bin\debug\netcoreapp2.2
> dotnet EmpDir.dll

--this will start the webserver and display directions to load the app in your browser, usually http://localhost:5000
	